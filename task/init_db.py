from sqlalchemy import create_engine, MetaData

from app.settings import BASE_DIR, get_config
from app.models import users, tasks
from app.security import generate_password_hash

DSN = "postgresql://{username}:{password}@{host}:{port}/{database}"
CONFIG_PATH = BASE_DIR / 'config' / 'test_config.yaml'


def create_tables(engine):
    meta = MetaData()
    meta.create_all(bind=engine, tables=[users, tasks])


def sample_data(engine):
    conn = engine.connect()
    conn.execute(users.insert(), [
        {
            'username': "admin",
            'password_hash': generate_password_hash('admin'),
            'email': 'admin@gmail.com',
            'phone_number': '88005553535'
        }
    ])
    conn.execute(tasks.insert(), [
        {'title': 'task 1', 'description': 'description for task 1', 'user_id': 1},
        {'title': 'task 2', 'description': 'description for task 2', 'user_id': 1},
        {'title': 'task 3', 'description': 'description for task 3', 'user_id': 1},
    ])
    conn.close()


if __name__ == '__main__':
    db_url = DSN.format(**get_config(CONFIG_PATH)['postgres'])
    engine = create_engine(db_url)

    create_tables(engine)
    sample_data(engine)
