import pathlib
import yaml
import os

import aiohttp_jinja2
import jinja2

BASE_DIR = pathlib.Path(__file__).parent.parent
PACKAGE_NAME = 'task.app'

STATIC_ROOT = '/static/'
STATICFILES_DIR = os.path.join(BASE_DIR, 'app/static')


def get_config(config_path):
    with open(config_path) as f:
        config = yaml.safe_load(f)
    return config

