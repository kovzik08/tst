from aiohttp import web
from aiohttp_security import authorized_userid, forget, remember
import aiohttp_jinja2

from app.models import get_user_by_name, get_users_tasks
from app.forms import validate_form_data


def redirect(router, route_name):
    location = router[route_name].url_for()
    return web.HTTPFound(location)


@aiohttp_jinja2.template('index.html')
async def index(request):
    print('INDEX')
    username = await authorized_userid(request)
    if not username:
        raise redirect(request.app.router, 'login')

    async with request.app['psql_pool'].acquire() as conn:
        current_user = await get_user_by_name(conn, username)
        tasks = await get_users_tasks(conn, current_user)

    return {'user': current_user, 'tasks': tasks}


@aiohttp_jinja2.template('login.html')
async def login(request):
    print('LOGIN')
    username = await authorized_userid(request)
    if username:
        raise redirect(request.app.router, 'index')

    if request.method == 'POST':
        form = await request.post()
        print(form.items())
        async with request.app['psql_pool'].acquire() as conn:
            error = await validate_form_data(conn, form)
            if error:
                return {'error': error}
            else:
                response = redirect(request.app.router, 'index')
                user = await get_user_by_name(conn, form['username'])
                await remember(request, response, user['username'])
                raise response
    return {}


async def logout(request):
    response = redirect(request.app.router, 'login')
    await forget(request, response)
    return response
