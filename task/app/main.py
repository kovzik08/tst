from aiohttp_session.cookie_storage import EncryptedCookieStorage
from cryptography import fernet

from app.routes import setup_routes
from app.settings import get_config, BASE_DIR
from app.db import init_db, init_redis
from app.db_auth import DBAuthorizationPolicy
from app.utils import current_user_processor

import jinja2
import os
# import base64

from aiohttp import web
from aiohttp_jinja2 import setup as setup_template
from aiohttp_security import setup as setup_security
from aiohttp_security import SessionIdentityPolicy
from aiohttp_session import setup as setup_session
from aiohttp_session.redis_storage import RedisStorage


async def init_app(config):
    app = web.Application()
    app['config'] = config
    setup_routes(app)
    psql_pool = await init_db(app)
    redis_pool = await init_redis(app)
    setup_session(app, RedisStorage(redis_pool))
    # fernet_key = fernet.Fernet.generate_key()
    # secret_key = base64.urlsafe_b64decode(fernet_key)
    # setup_session(app, EncryptedCookieStorage(secret_key))
    setup_template(
        app,
        loader=jinja2.FileSystemLoader(os.path.join(os.getcwd(), 'templates')),
        context_processors=[current_user_processor]
    )
    setup_security(app, SessionIdentityPolicy(), DBAuthorizationPolicy(psql_pool))
    return app


if __name__ == '__main__':
    config = get_config(BASE_DIR / 'config' / 'test_config.yaml')
    app = init_app(config)
    web.run_app(app, host='localhost', port=8000)
