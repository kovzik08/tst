import settings
import views


def setup_routes(app):
    app.router.add_static(settings.STATIC_ROOT, path=settings.STATICFILES_DIR, name='static')
    app.router.add_get('/login', views.login, name='login')
    app.router.add_post('/login', views.login, name='login')
    app.router.add_get('/logout', views.logout, name='logout')
    app.router.add_get('/', views.index, name='index')
