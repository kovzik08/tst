import datetime

from sqlalchemy import (
    Table, Column, Integer, MetaData,
    String, DateTime, ForeignKey, Text,
)

from sqlalchemy.sql import select

meta = MetaData()

users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('username', String(64), nullable=False, unique=True),
    Column('password_hash', String(128), nullable=False),
    Column('email', String(200), unique=True, nullable=False),
    Column('phone_number', String(25), unique=True),
)

tasks = Table(
    'tasks', meta,
    Column('id', Integer, primary_key=True),
    Column('title', String(150), nullable=False),
    Column('description', Text, nullable=False),
    Column('pub_date', DateTime, default=datetime.datetime.utcnow(), onupdate=datetime.datetime.utcnow()),
    Column('user_id', Integer, ForeignKey('users.id', ondelete='CASCADE'))
)


class RecordNotFound(Exception):
    pass


async def get_user_by_name(conn, username):

    result = await conn.fetchrow(
        users.select().where(users.c.username == username)
    )
    return result


async def get_users_tasks(conn, user):
    results = await conn.execute(tasks.select().where(tasks.c.user_id == user.c.id))
    records = await results.fetchall()
    if not records:
        msg = 'You dont have any task'
        raise RecordNotFound(msg)
    return records

#
# async def get_question(conn, question_id):
#     result = await conn.execute(questions.select().where(questions.c.id == question_id))
#     question_record = await result.first()
#     if not question_record:
#         msg = "Question with id: {} does not exists"
#         raise RecordNotFound(msg.format(question_id))
#     result = await conn.execute(
#         choices.select().where(choices.c.question_id == question_id).order_by(choices.c.id)
#     )
#     choice_records = await result.fetchall()
#     return question_record, choice_records
#
#
# async def vote(conn, question_id, choice_id):
#     result = await conn.execute(
#         choices.update().returning(*choices.c).where(choices.c.question_id == question_id)
#             .where(choices.c.id == choice_id).values(votes=choices.c.votes + 1)
#     )
#     record = await result.fetchone()
#     if not record:
#         msg = "Question with id: {} or choice with id: {} does not exists"
#         raise RecordNotFound(msg.format(question_id, choice_id))
