from app.security import check_password_hash
from app.models import get_user_by_name


async def validate_form_data(conn, form):
    username = form['username']
    password = form['password']

    if not username:
        return "Username is required"
    if not password:
        return "Password is require"

    user = await get_user_by_name(conn, username)

    if not user:
        return "Invalid username"
    if not check_password_hash(password, user['password_hash']):
        return "Invalid password"
    else:
        return None

