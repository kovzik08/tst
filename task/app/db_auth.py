from aiohttp_security import AbstractAuthorizationPolicy
from app.models import get_user_by_name


class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, psql_pool):
        self.psql_pool = psql_pool

    async def authorized_userid(self, identity):
        async with self.psql_pool.acquire() as conn:
            user = await get_user_by_name(conn, identity)
            if user:
                return identity
        return None

    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False
        return True
