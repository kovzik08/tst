import asyncpgsa
import aioredis


async def init_db(app):
    dsn = construct_db_url(app['config']['postgres'])
    psql_pool = await asyncpgsa.create_pool(dsn=dsn)
    app['psql_pool'] = psql_pool
    return psql_pool


async def init_redis(app):
    pool = await aioredis.create_pool((
        app['config']['redis']['host'],
        app['config']['redis']['port']
    ))

    async def close_redis(app):
        pool.close()
        await pool.wait_closed()

    app.on_cleanup.append(close_redis)
    app['redis_pool'] = pool
    return pool


def construct_db_url(config):
    DSN = "postgresql://{username}:{password}@{host}:{port}/{database}"
    return DSN.format(
        username=config['username'],
        password=config['password'],
        database=config['database'],
        host=config['host'],
        port=config['port']
    )


async def close_pg(app):
    app['database'].close()
    await app['database'].wait_closed()
